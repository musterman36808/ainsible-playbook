# ainsible playbook

## Chapter 1

Im Ordner *vagrant up* ausführen. Um anschliessend die Keys auf den Nodes
**ansible02** und **ansible03** zu deployen muss auf dem Host **master**
ein Key erstellt werden

```bash
ssh-keygen

```
mit ssh-copy-id kann der Public Key dann auf den anderen Servern ausgerollt werden.

Damit ssh-cpoy angewendet werden kann muss aber noch im ssh config file von
ansible02 und ansible03 /etc/ssh/sshd_config

die Zeile mit: *PasswortAuthentication no* zu *PasswordAuthentication yes* geändert werden.

```bash
ssh-copy-id -i .ssh/id_rsa.pub vagrant@192.168.50.30
ssh-copy-id -i .ssh/id_rsa.pub vagrant@192.168.50.20
```
Zuguter letzt müssen noch im */etc/ansible/hosts/* file auf dem master
noch die Hosts hinzugefügt werden. Dazu werden im Hostfile folgende Zeilen eingefügt.

```yaml
[srv_nodes]
192.168.0.20
192.168.0.30
```

Jetzt sollte der ansible Ping Befehl funtkionieren. 

*ansible srv_nodes -m ping*

## Chapter 2
Playbook für Apache Webserver aufzusetzen ist unter Files vermerkt. Auf
dem master muss dazu ein Playbook erstellt werden. Dazu muss der Text aus dem
playbook.yaml auf dem yaml File auf das unten erstellte File auf dem master kopiert werden.

```bash
sudo touch /etc/ansible/playbook.yml
sudo vi /etc/ansible/playbook.yml 
```
**für Ubuntu heisst das Webserver Paket nicht HTTPD sondern Apache2**

Anschliessend das Playbookfile von files auf ansible02 in */etc/ansible/playbook.yml* kopieren.

Das Playbook kann mit ansible auf syntax gecheckt und anschliessend ausgeführt werden

```bash
#yaml file syntax check
ansible-playbook /etdc/ansible/playbook.yml --check 
# run playbook
ansible-playbook /etdc/ansible/playbook.yml
```
Um zu überprüfen ob das Playbook erfolgreich durchlief: 

ansible02: cat var/www/html/index.html sollte dann der Text *"This is the first ansible Website"* erscheinen
ansible02: systemctl status apache 2 sollte dann der Status *running* erscheinen

## Chapter 3 

Um den Webserver auf dem Host zu erreichen muss ein private Network konfiguriert werden in Vagrant. 
Da dies schon in den vorigen Vagrant Files so spezifiziert wurde, muss hier keine weitere Konfiguration
vorgenommen werden.

```ruby
vagrant reload ansible02
```
evtl muss dann auf dem *ansible02* der Service nochmals neu gestartet werden:

```bash
systemctl start apache2
```
Über den Link [192.168.50.20](192.168.50.20) sollte die Webseite nun ersichtlich sein
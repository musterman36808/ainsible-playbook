#!/bin/sh -eux

sudo apt update & sudo apt upgrade -y

sudo apt-get install python -y 
sudo apt-get install python-pip -y
sudo apt-get install ansible -y

#ssh connection
sudo apt-get install sshpass -y 
